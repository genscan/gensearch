import { Refine } from "@pankod/refine-core"
import {
    notificationProvider,
    Layout,
    ErrorComponent,
} from "@pankod/refine-antd"
import routerProvider, {HashRouterComponent} from "@pankod/refine-react-router-v6"
import "@pankod/refine-antd/dist/reset.css"

// custom layout/Title
import {
    Title,
} from "./components/layout/title"


import { ConceptShow, ConceptList } from "./pages/concept"

// @ts-ignore
import { GraphQLClient as CustomGraphQLClient, default as customGraphqlDataProvider } from 'refine-app-search'


// for subdirector serving in prod
// @TODO: see how to activate only in prod

const { RouterComponent } = routerProvider
// CRA configuration for subfolder in prod, but root in dev: https://skryvets.com/blog/2018/09/20/an-elegant-solution-of-deploying-react-app-into-a-subdirectory/
// use HashRouter for gitlab page (avoid _redirects and 404 tweaks)
const CustomRouterComponent = () => <HashRouterComponent /> // basename={process.env.PUBLIC_URL} />;

const customRouterProvider = {
    ...routerProvider,
    RouterComponent: CustomRouterComponent,
    // RouterComponent: CustomRouterComponent.bind({
    //     initialRoute: 'Formations',
    // }),
}
// when no publishing, use : 
// const customRouterProvider = routerProvider

// @TODO: review naming of this function, like appSearchClient 
const customAPI_URL = 'http://localhost:5000'
const customClient = new CustomGraphQLClient(customAPI_URL)
const customGqlDataProvider = customGraphqlDataProvider(customClient)

const multipleDataProvider = {
    default: customGqlDataProvider,
    rdfx: customGqlDataProvider,
}

// This is for now required to start rdfx-graphql 
// cd ~/dev/mindmatcher/mmorg/ismene/rdfx-graphql/ ; npm start
const App: React.FC = () => {
    return (
        <Refine
            // dataProvider={dataProvider(API_URL)}
            dataProvider={multipleDataProvider}
            routerProvider={customRouterProvider}
            resources={[
                {
                    name: 'Formations',
                    options: {
                        dataProviderName: 'rdfx'
                    },
                    list: ConceptList,
                    show: ConceptShow,
                }
            ]}
            notificationProvider={notificationProvider}
            Layout={Layout}
            Title={Title}
            catchAll={<ErrorComponent />}
            options={{
                syncWithLocation: true,
            }}
        />
    );
};

export default App;
