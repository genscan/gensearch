import React, { useState } from 'react'
import { IResourceComponentsProps, HttpError } from "@pankod/refine-core";
import type { CrudFilters } from '@pankod/refine-core'
import {
    List,
    Table,
    useTable,
    Space,
    ShowButton,
    getDefaultSortOrder,
    Select,
    TextField,
    TagField,
    Form,
    SaveButton,
    Input,
    Row,
    Col,
} from "@pankod/refine-antd"

import { IPost } from './interfaces'
interface ISearch {
    title: string;
}

export const ConceptList: React.FC<IResourceComponentsProps> = () => {

    const facets = [
        {
            prefLabel: 'Organisme',
            elastic: {
                name: 'training_provider',
                size: 10000,
            }
        },
        {
            prefLabel: 'Status',
            elastic: {
                name: 'tags___state',
            }
        },
        {
            prefLabel: 'Tags',
            elastic: {
                name: 'tags___label',
            },
            col: {
                span: 9,
            }
        },
    ]

    const [facetValues, setFacetValues] = useState({})
    const { tableProps, sorter, searchFormProps, filters } = useTable<IPost, HttpError, ISearch>({
        metaData: {
            facets,
            setFacetValues,
        },
        onSearch: (values) => {
            const filterParams: CrudFilters = Object.entries(values).map(([field, value]) => ({
                field,
                operator: 'contains',
                value,
            }))
            return filterParams

        },
    });

    // Search Management
    const filterKey_name = 'title'

    // Facets management
    //@ts-ignore
    const filterConf = filters?.find(f => f.field === filterKey_name)
    const defaultValue_title = filterConf?.value ?? ''

    const facetSelectConf = facets.map(facet => {
        // 1/ Options 
        //@ts-ignore
        const options = facetValues[facet.elastic.name] ? facetValues[facet.elastic.name].map(f => ({
            // @ts-ignore
            label: `${f.value} - ${f.count}`,
            // @ts-ignore
            value: f.value,
        })) : []

        // 2/ defaultValues
        //@ts-ignore
        const facetFilter = filters?.find(f => f.field === facet.elastic.name)
        const defaultValue = facetFilter?.value ?? []

        const formParam = {
            name: facet.elastic.name,
            label: facet.prefLabel,
        }

        const selectParam = { options, defaultValue }

        const colParam = Object.assign({}, { span: 8 }, facet.col ?? {})

        return { formParam, selectParam, colParam }
    })

    const getSelects = () => facetSelectConf.map((selectConf, i) => {
        return (
            <Col key={`get-select-${i}`} {...selectConf.colParam}>
                <Form.Item {...selectConf.formParam} >
                    <Select
                        mode='multiple'
                        allowClear
                        placeholder='Please select'
                        {...selectConf.selectParam}
                        // size='large'
                        maxLength={800}

                    />
                </Form.Item>
            </Col>
        )
    })

    return (
        <List>
            <Form {...searchFormProps} >
                <Row justify="space-evenly" gutter={24}>
                    <Col span={4}>
                        <Form.Item name="title" label='Titre'>
                            <Input placeholder="Search by title" defaultValue={defaultValue_title} />
                        </Form.Item>
                    </Col>
                    {getSelects()}

                    <SaveButton onClick={searchFormProps.form?.submit}>Valider</SaveButton>
                </Row>
            </Form>

            <Table {...tableProps} rowKey="id">
                <Table.Column
                    key="id"
                    title="ID"
                    sorter={{ multiple: 2 }}
                    defaultSortOrder={getDefaultSortOrder("id", sorter)}
                    render={(value) => {
                        const idValue = value.id ? value.id : '___No ID___'
                        const textValue = `${idValue.slice(0, 9)}...`
                        return (<TextField value={textValue} />)
                    }}
                />
                <Table.Column
                    key="title"
                    title="Titre"
                    sorter={{ multiple: 1 }}
                    render={(value) => {
                        // const textValue = value.prefLabel[0].value ? value.prefLabel[0].value : 'No prefLabel'
                        const textValue = value.title ? value.title : '__sans_titre__'
                        return (<TextField value={textValue} />)
                    }}
                />

                <Table.Column
                    // key="title"
                    title="Organisme"
                    sorter={{ multiple: 1 }}
                    render={(value) => {
                        const textValue = value.training_provider ?? '__undefined__'
                        return (<TextField value={textValue} />)
                    }}
                />

                <Table.Column
                    title="Status"
                    sorter={{ multiple: 1 }}
                    render={(value) => {
                        // @ts-ignore
                        const tags = value.tags___state
                        // @ts-ignore
                        return tags ? tags.map((t, i) => <TagField key={`broader-${i}`} value={t} />) : ''
                    }}
                />

                <Table.Column
                    title="Tags"
                    sorter={{ multiple: 1 }}
                    render={(value) => {
                        // @ts-ignore
                        const tags = value.tags___label
                        // @ts-ignore
                        return tags ? tags.map((t, i) => <TagField key={`broader-${i}`} value={t} />) : ''
                    }}
                />


                <Table.Column<IPost>
                    title="Actions"
                    dataIndex="actions"
                    render={(_, record) => (
                        <Space>
                            {/* <EditButton
                                hideText
                                size="small"
                                recordItemId={record.id}
                            /> */}
                            <ShowButton
                                hideText
                                size="small"
                                recordItemId={record.id}
                            />
                            {/* <DeleteButton
                                hideText
                                size="small"
                                recordItemId={record.id}
                            /> */}
                        </Space>
                    )}
                />
            </Table>
        </List>
    );
};
