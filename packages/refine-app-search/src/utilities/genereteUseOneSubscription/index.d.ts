import { MetaDataQuery, BaseKey } from "@pankod/refine-core";
type GenereteUseOneSubscriptionParams = {
    resource: string;
    metaData: MetaDataQuery;
    id?: BaseKey;
};
type GenereteUseOneSubscriptionReturnValues = {
    variables: any;
    query: string;
    operation: string;
};
export declare const genereteUseOneSubscription: ({ resource, metaData, id, }: GenereteUseOneSubscriptionParams) => GenereteUseOneSubscriptionReturnValues;
export {};
