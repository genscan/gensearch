import { MetaDataQuery, BaseKey } from "@pankod/refine-core";
type GenereteUseManySubscriptionParams = {
    resource: string;
    metaData: MetaDataQuery;
    ids?: BaseKey[];
};
type GenereteUseManySubscriptionReturnValues = {
    variables: any;
    query: string;
    operation: string;
};
export declare const genereteUseManySubscription: ({ resource, metaData, ids, }: GenereteUseManySubscriptionParams) => GenereteUseManySubscriptionReturnValues;
export {};
