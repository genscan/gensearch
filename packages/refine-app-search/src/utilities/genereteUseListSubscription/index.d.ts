import { MetaDataQuery, Pagination, CrudSorting, CrudFilters } from "@pankod/refine-core";
type GenereteUseListSubscriptionParams = {
    resource: string;
    metaData: MetaDataQuery;
    pagination?: Pagination;
    hasPagination?: boolean;
    sort?: CrudSorting;
    filters?: CrudFilters;
};
type GenereteUseListSubscriptionReturnValues = {
    variables: any;
    query: string;
    operation: string;
};
export declare const genereteUseListSubscription: ({ resource, metaData, pagination, hasPagination, sort, filters, }: GenereteUseListSubscriptionParams) => GenereteUseListSubscriptionReturnValues;
export {};
