import { CrudFilters, CrudSorting, DataProvider } from "@pankod/refine-core";
import { GraphQLClient } from "graphql-request";
export declare const genereteSort: (sort?: CrudSorting) => string | never[];
export declare const generateFilter: (filters?: CrudFilters) => {
    [key: string]: any;
};
declare const dataProvider: (client: GraphQLClient) => Required<DataProvider>;
export default dataProvider;
