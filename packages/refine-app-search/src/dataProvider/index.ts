import {
    CrudFilters,
    CrudSorting,
    DataProvider,
    LogicalFilter,
} from "@pankod/refine-core"
import { GraphQLClient } from "graphql-request";

// @ts-ignore
import * as ElasticAppSearch from "@elastic/app-search-javascript";
// @ts-ignore
import elasticConf from '../conf/elasticConf.ts'

// @TODO: check the in-progress Elastic "universal client" (for node and the browser). As of 20230210 it's not released
//  https://github.com/elastic/enterprise-search-js/blob/main/packages/enterprise-search-universal

export const genereteSort = (sort?: CrudSorting) => {
    console.warn('@TODO: implement the genereteSort')
    if (sort && sort.length > 0) {
        //@ts-ignore
        const sortQuery = sort.map((i) => {
            return `${i.field}:${i.order}`;
        });

        return sortQuery.join();
    }

    return [];
};

export const generateFilter = (filters?: CrudFilters) => {
    console.warn('@TODO: implement the generateFilter')
    const queryFilters: { [key: string]: any } = {};

    if (filters) {
        //@ts-ignore
        filters.map((filter) => {
            if (
                filter.operator !== "or" &&
                filter.operator !== "and" &&
                "field" in filter
            ) {
                const { field, operator, value } = filter;

                if (operator === "eq") {
                    queryFilters[`${field}`] = value;
                } else {
                    queryFilters[`${field}_${operator}`] = value;
                }
            } else {
                const value = filter.value as LogicalFilter[];

                const orFilters: any[] = [];
                value.map((val) => {
                    orFilters.push({
                        [`${val.field}_${val.operator}`]: val.value,
                    });
                });

                queryFilters["_or"] = orFilters;
            }
        });
    }

    return queryFilters;
};

// @ts-ignore
function getAppSearchClient(conf) {
    const { apiKey, baseUrl, engineName } = conf
    var client = ElasticAppSearch.createClient({
        searchKey: apiKey,
        endpointBase: baseUrl,
        engineName,
    })
    return client
}

//@ts-ignore
function elastic2js(elasticArray) {
    //@ts-ignore
    return elasticArray.map(elasticObject => {
        const newEntries = Object.entries(elasticObject).map(([key, value]) => {
            if (key === '_meta') return []
            //@ts-ignore
            return [key, value.raw]
        })

        return Object.fromEntries(newEntries)
    })
}


const url = 'https://ce49528ce58a42c0a70dd416ef2baf33.ent-search.europe-west3.gcp.cloud.es.io'

const dataProvider = (client: GraphQLClient): Required<DataProvider> => {
    const appsearch_client = getAppSearchClient(elasticConf.stock_current)

    return {
        getList: async ({
            pagination = { current: 1, pageSize: 10 },
            //@ts-ignore
            filters,
            //@ts-ignore
            metaData,
        }) => {

            const { current, pageSize } = pagination

            const facets = metaData?.facets ?? []
            //@ts-ignore
            const facets_map = facets.reduce((acc, facet) => {
                acc.set(facet.elastic.name, facet)
                return acc
            }, new Map())


            //@ts-ignore
            const facets_filters = filters.filter(f => [...facets_map.keys()].includes(f.field))
            //@ts-ignore
            const elasticFilter = facets_filters.reduce((acc, filter) => {
                if (!filter) return acc
                if(!filter.value.length) return acc
                acc[filter.field] = filter.value
                return acc
            }, {})

            const defaultElasticFacet = {
                type: 'value',
                name: '__default__name__',
                sort: { count: 'desc' },
                size: 50,
            }
            const elasticFacets = {}
            for (const [facetName, facetConf] of facets_map) {
                //@ts-ignore
                elasticFacets[facetName] = Object.assign({}, defaultElasticFacet, facetConf.elastic)
            }


            // Text filter management 
            //@ts-ignore
            const titleFilter = filters.find(f => f.field === 'title')
            const query = titleFilter?.value ?? ''

            var options = {
                query,
                search_fields: { title: {} },
                filters: elasticFilter,
                // result_fields: { id: { raw: {} }, title: { raw: {} } }
                page: {
                    size: pageSize,
                    current,
                },
                facets: elasticFacets,
            }

            const searchResults = await appsearch_client.search('', options)

            // facets results management 
            const facetValues = [...facets_map.keys()].reduce( (acc,facetName) => {
                const values = searchResults.info.facets[facetName][0]?.data ?? []
                acc[facetName] = values
                return acc
            }, {})
            if (metaData.setFacetValues) {
                metaData.setFacetValues(facetValues)
            }

            // data management
            const data = elastic2js(searchResults.rawResults)
            const total = searchResults.info.meta.page.total_results

            return {
                data, 
                total,
            }
        },

        // @ts-ignore
        getMany: async ({ resource, ids, metaData }) => {
            console.warn('@TODO: implement getMany')
        },
        // @ts-ignore
        create: async ({ resource, variables, metaData }) => {
            console.warn('@TODO: implement the create')
        },
        // @ts-ignore
        createMany: async ({ resource, variables, metaData }) => {
            console.warn('@TODO: implement the createMany')
        },
        // @ts-ignore
        update: async ({ resource, id, variables, metaData }) => {
            console.warn('@TODO: implement the update')
        },
        // @ts-ignore
        updateMany: async ({ resource, ids, variables, metaData }) => {
            console.warn('@TODO: implement the updateMany')
        },
        // @ts-ignore
        getOne: async ({ resource, id, metaData }) => {
            console.warn('@TODO: implement the getOne')
        },
        // @ts-ignore
        deleteOne: async ({ resource, id, metaData }) => {
            console.warn('@TODO: implement the deleteOne')
        },
        // @ts-ignore
        deleteMany: async ({ resource, ids, metaData }) => {
            console.warn('@TODO: implement the deleteMany')
        },
        // @ts-ignore
        getApiUrl: () => {
            console.warn('@TODO: implement the getApiUrl')
        },
        // @ts-ignore
        custom: async ({ url, method, headers, metaData }) => {
            console.warn('@TODO: implement the custom')
        },
    };
};

export default dataProvider;
