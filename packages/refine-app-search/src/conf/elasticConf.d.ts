declare const elasticConf: {
    current: {
        apiKey: string;
        baseUrl: string;
        engineName: string;
    };
    prod: {
        apiKey: string;
        baseUrl: string;
        engineName: string;
    };
    selected: {
        apiKey: string;
        baseUrl: string;
        engineName: string;
    };
    preprod: {
        apiKey: string;
        baseUrl: string;
        engineName: string;
    };
    stock_current: {
        apiKey: string;
        baseUrl: string;
        engineName: string;
    };
    stock_prod: {
        apiKey: string;
        baseUrl: string;
        engineName: string;
    };
};
export default elasticConf;
