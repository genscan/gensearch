import { LiveProvider } from "@pankod/refine-core";
import { Client } from "graphql-ws";
export declare const liveProvider: (client: Client) => LiveProvider;
