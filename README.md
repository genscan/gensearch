# gensearch

Démo et documentation du moteur de recherche par défaut pour la GEN.


# development 

```
pnpm start 
pnpm dev
``` 

# build 

```
./build-artifact.sh
```


# local test 

* pour l'index.html et la version compilée de l'interface du moteur de recherche: 
```
cd apps/search_ui
npx serve build
```

# Publish 


* commit et push des modifications sur la branche `main`


# Checks : 

* de la pipeline: https://gitlab.com/genscan/gensearch/-/pipelines
* du site: https://genscan.gitlab.io/gensearch/
